/* global ASSETS_API */
import axios from 'axios'

export const fetchAssets = userId => axios.get(`${ASSETS_API}?u=${userId}`)
