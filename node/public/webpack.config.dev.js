const path = require('path')
const webpack = require('webpack')
require('dotenv').config({ path: './config/.env.client'})

module.exports = function (env) {
  return {
    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [
            {
              loader: 'style-loader'
            },
            {
              loader: 'css-loader',
              options: {
                sourceMap: true
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: [
                  // require('autoprefixer')
                ],
                sourceMap: true,
              }
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true
              }
            }
          ]
        }
      ]
    },
    devtool: 'cheap-source-map',
    devServer: {
      contentBase: path.join(__dirname, 'dist'),
      hot: true,
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: '"development"'
        },
        ASSETS_API: JSON.stringify(process.env.ASSETS_API_DEV),
        NODE_API: JSON.stringify(process.env.NODE_API_DEV)
      })
    ],
  }
}
