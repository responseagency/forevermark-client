import Vue from 'vue'
import PortalVue from 'portal-vue'
import ScrollView from './plugins/vue-scrollview'
import App from './App'
import store from './vuex'
import router from './router'

import '../assets/scss/main.scss'

Vue.use(PortalVue)
Vue.use(ScrollView)

/* eslint-disable no-new */
const root = new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App),
})

export default root
