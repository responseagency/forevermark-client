import { mapState } from 'vuex'

export default {
  name: 'Page',
  render() {
    return (
      <pre>
        {JSON.stringify(this.pages[this.page], null, 4)}
      </pre>
    )
  },
  computed: {
    ...mapState({
      pages: state => state.data.pages,
    }),
  },
  props: {
    page: {
      type: String,
      required: true,
    },
  },
}
