import { mapState, mapActions } from 'vuex'

import MainNavigation from './lib/MainNavigation'
import ScrollNavigation from './lib/ScrollNavigation'

export default {
  name: 'Navigation',
  render() {
    return (
      <div>
        {/* <transition onLeave={() => console.log('test')} duration={200}> */}
        <nav
          class={{
            'show-right': this.pageNav && this.showingRightNav,
            navigation: true,
          }}
        >
          <div class="control" v-show={this.pageNav}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="-536 -410 232 232"
              class="control-icon"
              onClick={() => this.toggleRightNav(!this.showingRightNav)}
            >
              <g id="1487195380_left_circle" fill="none" fill-rule="evenodd">
                <path
                  id="Path"
                  fill="#687278"
                  d="M-304.8-294c0 63.521-51.679 115.2-115.2 115.2-63.522 0-115.2-51.679-115.2-115.2 0-63.522 51.678-115.2 115.2-115.2 63.521 0 115.2 51.678 115.2 115.2z"
                />
                <path
                  id="Shape"
                  fill="#FFF"
                  d="M-398.026-230.854c-1.638 0-3.276-.625-4.524-1.875l-56.748-56.746c-2.5-2.499-2.5-6.552 0-9.05l56.748-56.747c2.496-2.5 6.553-2.5 9.049 0 2.5 2.499 2.5 6.552 0 9.05L-445.722-294l52.22 52.222c2.5 2.499 2.5 6.552 0 9.05-1.248 1.249-2.887 1.874-4.524 1.874z"
                />
              </g>
            </svg>
          </div>

          <div class="left-menu menu">
            <MainNavigation />
          </div>
          <div class="right-menu menu">
            <ScrollNavigation />
          </div>
        </nav>
        {/* </transition> */}
      </div>
    )
  },
  methods: {
    ...mapActions(['toggleRightNav']),
  },
  computed: {
    ...mapState({
      showingRightNav: state => state.navigation.showingRightNav,
    }),
  },
  props: {
    pageNav: {
      type: Boolean,
      default: () => false,
    },
  },
}
