import debounce from 'lodash.debounce'
import { mapState, mapActions } from 'vuex'

export default {
  name: 'Header',
  render() {
    return (
      <header class={{ header: true, shrunk: this.shrunk }}>
        <a href="/#/">
          <img src="https://res.cloudinary.com/response-mktg/image/upload/v1487781224/forevermark/misc/logo.png" alt="Forevermark Logo" />
        </a>
        <portal to="mobile-nav-button">
          <div class={{ 'mobile-nav-button': true, open: this.mobileOpen }} onClick={this.toggleMobileNav}>
            <span class="mobile-nav-icon"></span>
          </div>
        </portal>
      </header>
    )
  },
  data() {
    return {
      shrunk: false,
    }
  },
  methods: {
    ...mapActions([
      'toggleMobileNav',
    ]),
  },
  computed: {
    ...mapState({
      mobileOpen: state => state.navigation.mobileOpen,
    }),
  },
  created() {
    this.headerListener = window.addEventListener('scroll', debounce(() => {
      if (window.scrollY > 250) {
        this.shrunk = true
      } else {
        this.shrunk = false
      }
    }, 50))
  },
}
