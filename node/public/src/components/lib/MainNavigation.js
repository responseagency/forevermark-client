import { mapState, mapActions } from 'vuex'

export default {
  name: 'Main-Navigation',
  data() {
    return {
      showing: '',
    }
  },
  render(h) {
    return (
      <div class="main-navigation">
        {
          this.menu && this.menu.map(item => {
            return (
              <div class="menu-block">
                <header onClick={() => this.toggleShowing(item.title)}>
                  <div class={{ 'expand-icon': true, expanded: this.showing === item.title }}></div>
                  <h2 class="menu-block-title">{item.title}</h2>
                </header>
                <ul class="menu-block-items">
                  { this.showing === item.title &&
                    item.children.map(
                      child => (
                        <li onClick={() => this.toggleRightNav(true)}>
                          <router-link to={`/${item.title.toLowerCase()}/${child.slug}`}>
                            {child.title}
                          </router-link>
                        </li>
                      )
                    )
                  }
                </ul>
              </div>
            )
          })
        }
      </div>
    )
  },
  methods: {
    ...mapActions([
      'toggleRightNav',
    ]),
    toggleShowing(item) {
      this.showing = (this.showing !== item)
        ? item
        : ''
    },
  },
  computed: {
    ...mapState({
      menu: state => state.navigation.menu,
    }),
  },
}
