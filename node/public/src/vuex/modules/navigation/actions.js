import * as types from './mutation-types'

export const toggleMobileNav = ({ commit }) => {
  commit(types.TOGGLE_MOBILE_OPEN)
}

export const toggleRightNav = ({ commit }, show) => {
  if (show) {
    commit(types.SHOW_RIGHT_NAV)
  } else {
    commit(types.HIDE_RIGHT_NAV)
  }
}
