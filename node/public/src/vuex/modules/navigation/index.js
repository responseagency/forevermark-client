/* eslint no-shadow:0*/
import * as types from './mutation-types'
import * as actions from './actions'
// import * as getters from './getters'

const state = {
  menu: undefined,
  mobileOpen: false,
  showingRightNav: true,
}

const mutations = {
  [types.TOGGLE_MOBILE_OPEN](state) {
    state.mobileOpen = !state.mobileOpen
  },
  [types.SHOW_RIGHT_NAV](state) {
    state.showingRightNav = true
  },
  [types.HIDE_RIGHT_NAV](state) {
    state.showingRightNav = false
  },
  [types.BOOTSTRAP_MENU](state, menu) {
    state.menu = menu
  },
}

export default {
  state,
  mutations,
  actions,
  // getters,
}
