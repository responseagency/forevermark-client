/* eslint no-shadow:0*/
import * as types from './mutation-types'
import * as actions from './actions'

const state = {
  assets: {},
  pages: {},
}

const mutations = {
  [types.BOOTSTRAP_ASSETS](state, assets) {
    state.assets = assets
  },
  [types.BOOTSTRAP_PAGES](state, pages) {
    state.pages = pages
  },
}

export default {
  state,
  mutations,
  actions,
}
