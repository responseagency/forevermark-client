import * as types from './mutation-types'
import { fetchAssets } from '../../../../config/api'

export async function loadData({ rootState, commit }) {
  try {
    const user = await getSession(rootState.auth.okta)

    const { data: { assets, pages, menu } } = await fetchAssets(user.userId)

    commit(types.BOOTSTRAP_ASSETS, assets)
    commit(types.BOOTSTRAP_PAGES, pages)
    commit(types.BOOTSTRAP_MENU, menu)
    commit(types.SET_USER, user)
  } catch (e) {
    return false
  }
  return true
}

const getSession = (okta) => {
  return new Promise((resolve, reject) => {
    okta.session
      .get()
      .then((res) => {
        if (res.status !== 'ACTIVE') reject()
        else resolve(res)
      })
      .catch(() => reject())
  })
}
