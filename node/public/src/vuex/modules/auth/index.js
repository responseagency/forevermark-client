/* eslint no-shadow:0*/
import * as types from './mutation-types'
import * as actions from './actions'

const state = {
  okta: undefined,
  user: undefined,
}

const mutations = {
  [types.SET_OKTA](state, okta) {
    state.okta = okta
  },
  [types.SET_USER](state, user) {
    state.user = user
  },
}

export default {
  state,
  mutations,
  actions,
}
