import OktaAuth from '@okta/okta-auth-js'
import * as types from './mutation-types'

export const initOkta = ({ commit }) => {
  /* global OKTA_URL, OKTA_CLIENT_ID, OKTA_REDIRECT_URI */
  commit(
    types.SET_OKTA,
    new OktaAuth({
      url: OKTA_URL,
      clientId: OKTA_CLIENT_ID,
      redirectUri: OKTA_REDIRECT_URI,
    }),
  )
}
