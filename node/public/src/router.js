import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from './components/pages/Home'
import Login from './components/pages/Login'
import Page from './components/pages/Page'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    { path: '/', component: Home, name: 'home' },
    { path: '/login', component: Login, name: 'login' },
    { path: '/*/:page', component: Page, name: 'Page', props: true },
    { path: '*', redirect: '/' },
  ],
})

export default router
