import { mapState, mapActions } from 'vuex'
import axios from 'axios'

import StickyHeader from './components/lib/StickyHeader'
import Navigation from './components/Navigation'

export default {
  name: 'App',
  render() {
    return (
      <main id="app">
        <portal-target name="mobile-nav-button"></portal-target>
        <section class={{ layout: true, full: this.isFullWidth, 'mobile-nav-open': this.mobileOpen }}>
          <article class="left">
            <Navigation pageNav={this.isPageNav} />
          </article>
          <article class="right" style={{ transition: this.layoutTransition }}>
            <div class="right--inner">
              <router-view />
            </div>
          </article>
          <StickyHeader />
        </section>
      </main>
    )
  },
  data() {
    return {
      layoutTransition: '',
    }
  },
  watch: {
    $route(to, from) {
      this.setLayoutTranstion(to, from)
    },
  },
  methods: {
    ...mapActions([
      'loadData',
      'initOkta',
      'getSession',
    ]),
    async initApp() {
      this.initOkta()
      const loaded = await this.loadData()
      if (!loaded) this.$router.push({ path: '/login' })
    },
    async bootstrapData() {
      const { data: { assets, menu, pages } } = await axios.get('http://fmj.response.hosting/wp-json/resources/v2/all-assets')
      this.bootstrapAssets(assets)
      this.bootstrapMenu(menu)
      this.bootstrapPages(pages)
    },
    setLayoutTranstion(to, from) {
      if (from.name === 'home' || to.name === 'login') {
        this.layoutTransition = 'transform .2s ease-in-out'
      }
    },
  },
  computed: {
    ...mapState({
      mobileOpen: state => state.navigation.mobileOpen,
      okta: state => state.auth.okta,
    }),
    isPageNav() {
      const { name } = this.$route
      return name !== 'home' && name !== 'login'
    },
    isFullWidth() {
      return this.$route.name === 'login'
    },
  },
  created() {
    this.initApp()
  },
}
