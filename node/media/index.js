module.exports = {
  marketing: {
    'marketing-calendar': require('./marketing/marketing-calendar.json'),
    'brand-guidelines': require('./marketing/brand-guidelines.json'),
    'public-relations': require('./marketing/public-relations.json'),
    'strategic-marketing-plan': require('./marketing/strategic-marketing-plan.json')
  },
  campaigns: {
    'ever-us': require('./campaigns/ever-us.json'),
    'exceptionals': require('./campaigns/exceptionals.json'),
    'black-label': require('./campaigns/black-label.json'),
    'seize-the-day': require('./campaigns/seize-the-day.json'),
    'responsible-sourcing': require('./campaigns/responsible-sourcing.json'),
    'center-of-my-universe': require('./campaigns/center-of-my-universe.json'),
    'the-one': require('./campaigns/the-one.json'),
    'golden-diamonds': require('./campaigns/golden-diamonds.json'),
    'mothers-day': require('./campaigns/mothers-day.json'),
    'earth-day': require('./campaigns/earth-day.json'),
    'graduation': require('./campaigns/graduation.json')
  },
  'co-op': {
    'overview': require('./co-op/overview.json')
  }
}
