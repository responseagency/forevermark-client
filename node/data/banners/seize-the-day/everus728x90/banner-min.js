var adRepeat = 1,
	played = 1,
	adDuration = 15000;
	

function playAnimation(){
	var delay = 0;
	animateText(1);
	delay+=4500;
	setTimeout(function(){animateToCTAFrom(1)}, delay);
	delay+=2500;
	setTimeout(watchIfReplayIsNeed, delay);

}


function animateText(num){
	var frameAnimTimer = 0,
		wordCount = $('.text_f'+num+'>div.word').length,
		wordDelay = 220;

	for(var i = 0; i<wordCount; i++)
	{
		frameAnimTimer+=wordDelay;
		$('.text_f'+num+'>div.word').eq(i).animate({opacity:1, translateY:0}, 500, 'ease-out', null, frameAnimTimer);
	} 	
}


function animateToCTAFrom(num){
	var frameAnimTimer = 350; 
	var speed = 350;
	$('.text_f'+num).animate({opacity:0}, speed, 'ease-in', null);
	speed = 700;
	
	frameAnimTimer+=speed;

	$('.product').animate({opacity:1}, speed, 'ease-in-out', null,frameAnimTimer);
	frameAnimTimer+=speed;
	$('.cta').animate({opacity:1, translateY:0}, speed, 'ease-in-out', null, frameAnimTimer);
	frameAnimTimer+=speed;
	$('.logo').animate({opacity:1}, speed, 'ease-in-out', null, frameAnimTimer);
	$('.partner-logo').animate({opacity:1}, speed, 'ease-in-out', null, frameAnimTimer);
}



function watchIfReplayIsNeed(){
	
	if( played < adRepeat )
		setTimeout( replay, adDuration/adRepeat);
	else 
		showReplayButton(true);
}


function replay(manualreplay) 
{
	
	var fadeSpeed = 350;
		played = (manualreplay) ? 0 : played;
		played++,
		

	
	//Fade Everything
	$('#global-container > div').animate({opacity:0}, fadeSpeed);
	showReplayButton(false);

	//Reset banner and relaunch
	setTimeout(function(){
		$('*').attr('style','');
		playAnimation();
	}, fadeSpeed*1.25)

}

function setListeners(){
	//Add the click on the clicktag full area button
	$('#clicktag-button').on('click', function(){ window.open(clickTag); });

	//Add the click on the cta if the cta need a rollover and so, put on top of the clicktag
	$('.cta-button').on('click', function(){ window.open(clickTag); });
	//Add mouseEnter if JS is need in the rollover
	// $('.cta-button').on('mouseenter', function(){ });
	//Add mouseLeave if JS is need in the rollover
	// $('.cta-button').on('mouseleave', function(){ });
}

$(function()
{
	setListeners();
	playAnimation();
});


// @codekit-append "replay-button.js";

var rotateTimeout;

function showReplayButton(doit){
	console.log('showReplayButton(doit)', doit)
	var _do = (doit == undefined) ? true : doit;
	if(_do)
	{
		$('.replay-button').animate({opacity:0.8}, 300);
		$('.replay-button').on('mouseenter', onReplayRollOver);
		$('.replay-button').on('mouseleave', onReplayRollOut);
		$('.replay-button').on('click', onReplayClicked);
	}
	else
	{
		$('.replay-button').animate({opacity:0}, 100);
		$('.replay-button').off('mouseenter', onReplayRollOver);
		$('.replay-button').off('mouseleave', onReplayRollOut);
		$('.replay-button').off('click', onReplayClicked);
	}	
}

function onReplayClicked(){
	clearTimeout(rotateTimeout);
	replay(true);
}

function rotateButton(){
	$('.replay-button').animate({rotate:'360deg'}, 1000, 'linear');
	rotateTimeout = setTimeout(function(){$('.replay-button').animate({rotate:'0deg'}, 0, 'linear', rotateButton)}, 1020)
}
function onReplayRollOver(){
	$('.replay-button').attr('style','opacity:1');
	rotateButton(); 
}
function onReplayRollOut(){
	clearTimeout(rotateTimeout);
	$('.replay-button').animate({rotate:'0deg', opacity:0.8}, 150);
}


