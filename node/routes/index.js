const express = require('express')
const router = express.Router()
const axios = require('axios')

// middleware
//const permissions = require('../middleware/permissions')

// handlers
const redis = require('../handlers/redis')
const media = require('../handlers/media')
const { fetchUser, fetchData, handlePermissions } = require('./handlers')

router
  .get('/api/media/:folderName/:pageName/:fileName', (req, res) => {
    const url = media.getFile(
      req.params.folderName,
      req.params.pageName,
      req.params.fileName
    )
    media.streamFile(url, req, res)
  })
  .get('/api/clear-cache/:key', (req, res) => {
    const { key } = req.params
    redis.deleteItem(key).then(data => {
      if (data) res.send(`${key} successfully deleted`)
      else res.send(`${key} does not exist in cache`)
    })
  })
  .get('/api/all-assets', [fetchUser, fetchData, handlePermissions])

module.exports = router
