const axios = require('axios')
const redis = require('../handlers/redis')
const okta = require('../handlers/okta')

// fetches data from cache or wordpress
function fetchData(req, res, next) {
  if (!req.user) res.status(401).send('Unauthorized')

  redis.fetchItem('all-assets').then(data => {
    if (data) {
      console.log('using cache')
      req.assets = data
      next()
    } else {
      axios
        .get(process.env.WP_ASSETS_URL)
        .then(resp => {
          redis.client.set('all-assets', JSON.stringify(resp.data), 'EX', 3600) // cache for 1 hour
          req.assets = resp.data
          next()
        })
        .catch(err => {
          console.log(err)
          res.status(500).send(JSON.stringify(err))
        })
    }
  })
}

// fetches user profile from okta
function fetchUser(req, res, next) {
  const { u } = req.query
  // if no user id passed as query string, don't allow
  if (!u) res.status(401).send('Unauthorized')

  okta
    .fetchUser(u)
    .then(({ data }) => {
      const { hidden, readOnly } = data.profile
      // only pass through the user info that we care about
      req.user = { id: u, hidden, readOnly }
      next()
    })
    .catch(err => {
      res.status(401).send('Unauthorized')
    })
}

function handlePermissions(req, res, next) {
  // if user has no restrictions, send everything through

  // it's a string in redis so we need to parse it back to an object
  if (typeof req.assets === 'string') {
    req.assets = JSON.parse(req.assets)
  }

  if (req.user.hidden) {
    // remove hidden pages
    for (const page of req.user.hidden) {
      if (page in req.assets.pages) {
        delete req.assets.pages[page]
      }
    }
  }

  if (req.user.readOnly) {
    // set readonly flag
    for (const page of req.user.readOnly) {
      if (page in req.assets.pages) {
        req.assets.pages[page].readOnly = true
      }
    }
  }

  res.send(req.assets)
}

module.exports = { fetchData, fetchUser, handlePermissions }
