const redis = require('redis')
const isProduction = process.env.NODE_ENV === 'production'

const client = isProduction
  ? redis.createClient({ host: 'redis', port: 6379 })
  : redis.createClient()

function deleteItem(key) {
  return new Promise((resolve, reject) => {
    client.del(key, (err, data) => {
      if (err) reject(err)
      resolve(data)
    })
  })
}

function fetchItem(key) {
  return new Promise((resolve, reject) => {
    client.get(key, (err, data) => {
      if (err) reject(err)
      resolve(data)
    })
  })
}

module.exports = { client, deleteItem, fetchItem }
