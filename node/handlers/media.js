const request = require('request')
const media = require('../media')


function getFile(folderName, pageName, fileName) {
  if (media[folderName] && media[folderName][pageName]) {
    return media[folderName][pageName][fileName]
  }
  return false
}

function streamFile(url, req, res) {
  if (url) {
    res.set({ "Content-Disposition": `attachment; filename=${req.params.fileName}` })
    request(url).pipe(res)
  } else {
    return res.send('Something went wrong when downloading the file, please try again.')
  }
}

module.exports = { getFile, streamFile }
