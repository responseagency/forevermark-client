const axios = require('axios')

const usersApi = userId => `${process.env.OKTA_URL}/api/v1/users/${userId}`

function fetchUser(userId) {
  return axios({
    method: 'get',
    url: usersApi(userId),
    headers: { Authorization: `SSWS ${process.env.OKTA_TOKEN}` }
  })
}

module.exports = { fetchUser }
