module.exports = function(req, res, next) {
  // const origin = req.get('origin')
  // if (process.env.NODE_ENV === 'local') {
  //   res.header("Access-Control-Allow-Origin", "*")
  //   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  // } else if (apiConfig.local === origin || apiConfig.stage === origin || apiConfig.production === origin) {
  //   res.header("Access-Control-Allow-Origin", origin)
  //   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  // }
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, X-Access-Token")
  next()
}
