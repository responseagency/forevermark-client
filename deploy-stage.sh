#!/bin/bash

cd node/public;

npm run build;

cd ../../;

docker-machine env forevermark-production;

eval $(docker-machine env forevermark-production);

docker-compose build && docker-compose up -d;

docker rm -v $(docker ps -a -q -f status=exited) 2>&1;
docker rmi $(docker images -f "dangling=true" -q) 2>&1;
